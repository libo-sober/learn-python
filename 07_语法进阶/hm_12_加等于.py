def demo(num,num_list):
    print("函数开始")

    # num = num + num
    num += num
    # 列表变量执行加等于，不是相加再赋值
    # 本质上是在调用extend（）方法
    # num_list += num_list  # [1, 2, 3, 1, 2, 3]
    # 使用了赋值语句不会改变全局变量
    num_list = num_list + num_list  # [1, 2, 3]
    # num_list.extend(num_list)
    print(num)  # 18
    print(num_list)

    print("函数完成")


gl_num = 9
gl_num_list = [1, 2, 3]

demo(gl_num,gl_num_list)
print(gl_num)  # 9
print(gl_num_list)  # [1, 2, 3, 1, 2, 3]
