def demo1():

    # 定义一个局部变量
    # 1.出生在执行了下方代码之后
    # 2.死亡在函数允许结束后
    num = 10

    print("在demo1函数中的num值为：%d" % num)

def demo2():
    # print("%d" % num)
    num = 99
    print("在demo1函数中的num值为：%d" % num)

# 在函数内部定义的变量，不能在其他位置使用
# print("%d" % num)

demo1()

demo2()
