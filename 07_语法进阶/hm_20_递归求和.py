"""
需求
1.定义一个函数 sum_numbers
2.能够接收一个num的整数参数
3.计算1+2+...+num的结果
"""


def sum_numbers(num):
    """递归求和

    :param num: 1+2+...+num
    :return: 和
    """
    if num < 0:
        print("输入错误！")
    elif num == 0 or num == 1:
        return num
    else:
        return num + sum_numbers(num - 1)


print("sum = %d" % sum_numbers(5))
