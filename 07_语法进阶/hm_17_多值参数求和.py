"""
需求
1.定义一个sum_numbers,可以接收的 任意多个整数
2.功能要求：将传递的 所有数字累加 并且返回累加结果
"""

# def sum_numbers(args):


def sum_numbers(*args):
    sum = 0
    print(args)
    for n in args:
        sum += n

    return sum


# result = sum_numbers((1,2,3,4,5))
result = sum_numbers(1, 2, 3, 4, 5)

print(result)
