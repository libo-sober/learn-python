a = 6
b = 100

# 解法1：其他变量
# c = a
# a = b
# b = c

# print("a:%d b:%d" % (a, b))

# 解法2：不使用其他变量
# a = a + b
# b = a - b
# a = a - b

# print("a:%d b:%d" % (a, b))

# 解法3：python专有
# a, b = (b, a)
# 等号右边是一个元组，只是把元组的小括号给省略了
a, b = b, a

print("a:%d b:%d" % (a, b))
