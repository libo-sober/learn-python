# 全局变量、类、函数
# 注意：直接执行的代码不是向外界提供的工具


def say_hello():
    print("say hello")

# 如果是当前执行的程序，__name__是__main__
if __name__ == "__main__":
    print(__name__)
    # 文件被导入时，能够直接执行的代码不需要执行
    print("小明开发的模块")
    say_hello()
