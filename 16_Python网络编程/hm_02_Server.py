import socket

s = socket.socket()
host = socket.gethostname()
port = 12345

# 主动初始化TCP服务器连接，。一般address的格式为元组（hostname,port），
# 如果连接出错，返回socket.error错误。
s.connect((host, port))

# 接收TCP数据，数据以字符串形式返回，bufsize指定要接收的最大数据量。
# flag提供有关消息的其他信息，通常可以忽略。
# 需要进行编码格式转换 不然会收不到所发的内容
print(s.recv(1024).decode())
# Python decode() 方法以 encoding 指定的编aA码格式解码字符串。默认编码为字符串编码。

s.close()
