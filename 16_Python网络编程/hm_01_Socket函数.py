# 什么是Socket？
# Socket又称"套接字"，应用程序通常通过“套接字”向网络发出
# 请求或者应答网络中的请求，
# 使主机间或者一台计算机上的进程间可以通讯。
# Python中我们用Socket函数来创建套接字
# socket.socket([family[,type[,proto]]])
# family: 套接字家族可以使AF_UNIX或者AF_INET
# type: 套接字类型可以根据是面向连接的还是非连接分为SOCK_STREAM或SOCK_DGRAM
# protocol: 一般不填默认为0

import socket  # 导入socket模块

s = socket.socket()  # 创建socket对象
host = socket.gethostname()  # 获取本地主机名
port = 12345  # 设置端口号
# 绑定地址（host,port）到套接字， 在AF_INET下,以元组（host,port）的形式表示地址。
s.bind((host, port))  # 绑定端口

# 开始TCP监听。backlog指定在拒绝连接之前，操作系统可以挂起的最大连接数量。
# 该值至少为1，大部分应用程序设为5就可以了。
s.listen(5)  # 等待客户端连接

msg = '欢迎访问菜鸟教程！'  #strip默认取出字符串的头尾空格

while True:
    # 被动接受TCP客户端连接, (阻塞式)
    # 等待连接的到来
    c,addr = s.accept()  # 建立客户端连接
    print ('连接地址：', addr)
    c.send(msg.encode('utf-8'))
    c.close()  # 关闭连接


