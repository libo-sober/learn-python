"""
题目：
有四个数字：1、2、3、4，
能组成多少个互不相同且无重复数字的三位数？
各是多少？
"""

n = 0

for i in (1, 2, 3, 4):
    for j in (2, 3, 4):
        for k in (3, 4):
            print(i*100+j*10+k)
            n += 1

print("能组成%d个互不相同且无重复数字的三位数" % n)


# 参考答案：
# for i in range(1,5):
#     for j in range(1,5):
#         for k in range(1,5):
#             if( i != k ) and (i != j) and (j != k):
#                 print i,j,k
# range()函数
# 语法：
# range(start, stop[, step])
# 参数说明：
#     start: 计数从 start 开始。默认是从 0 开始。例如range（5）等价于range（0， 5）;
#     stop: 计数到 stop 结束，但不包括 stop。例如：range（0， 5） 是[0, 1, 2, 3, 4]没有5
#     step：步长，默认为1。例如：range（0， 5） 等价于 range(0, 5, 1)
