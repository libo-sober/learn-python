# 1.打开文件
file = open("README", "r")

# 文件名
print("文件名：%s" % file.name)
print("文件是否已经关闭：%s" % file.closed)
print("访问模式：%s" % file.mode)
print()
# 2.读取文件内容
text = file.read(10)
print(text)
print(len(text))
print("*" * 50)

# 读取到哪儿，文件指针就会相应移动到哪里

text = file.read()
print(text)
print(len(text))

# 3.关闭文件
file.close()
