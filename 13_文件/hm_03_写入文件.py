# 1.打开文件
file = open("README", "a")

# 2.写入文件
# write()方法不会在字符串的结尾添加换行符
file.write("hello")

# 3.关闭文件
file.close()
