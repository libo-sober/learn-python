import smtplib
# python的smtplib提供了一种很方便的途径发送电子邮件。
# 它对smtp协议进行了简单的封装。
# 语法  smtpObj = smtplib.SMTP( [host [, port [, local_hostname]]] )
# host: SMTP 服务器主机。 你可以指定主机的ip地址或者域名如: runoob.com，这个是可选参数。
# port: 如果你提供了 host 参数, 你需要指定 SMTP 服务使用的端口号，一般情况下 SMTP 端口号为25。
# local_hostname: 如果 SMTP 在你的本机上，你只需要指定服务器地址为 localhost 即可
# Python SMTP 对象使用 sendmail 方法发送邮件，语法如下：
# SMTP.sendmail(from_addr, to_addrs, msg[, mail_options, rcpt_options])
# from_addr: 邮件发送者地址。
# to_addrs: 字符串列表，邮件发送地址。
# msg: 发送消息