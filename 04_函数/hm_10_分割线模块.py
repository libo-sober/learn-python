def print_line(char, times):

    print("%s  " % char * times,)


def print_lines(count, char, times):
    """打印多条分割线

    :param count: 行数
    :param char: 分割线所用字符
    :param times: 重复次数
    """
    row = 0

    while row < count:

        print_line(char, times)

        row += 1


name = "李波"
