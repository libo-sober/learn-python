name = "小明"

# 注意：
#    定义定义好函数后只表示函数封装了一段代码而已
# 如果不主动调用函数，函数是不会主动执行的


def say_hello():
    """打招呼"""

    print("hello 1")
    print("hello 2")
    print("hello 3")

print(name)

say_hello()

print(name)