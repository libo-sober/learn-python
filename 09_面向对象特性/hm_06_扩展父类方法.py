class Animal:
    def eat(self):
        print("吃---")

    def drink(self):
        print("喝---")

    def run(self):
        print("跑---")

    def sleep(self):
        print("睡---")


class Dog(Animal):
    # def eat(self):
    #     print("吃")
    #
    # def drink(self):
    #     print("喝")
    #
    # def run(self):
    #     print("跑")
    #
    # def sleep(self):
    #     print("睡")

    def bark(self):
        print("汪汪叫")


class XiaoTianQuan(Dog):
    def bark(self):
        # 1.针对子类特有的需求，编写代码
        print("神一样的叫唤...")
        # 2.使用super（）调用父类方法
        # super().bark()
        # 父类名.方法（self）
        Dog.bark(self)
        # 3.增加其他代码
        print("%￥&&*%￥%#%……&T*")

    def fly(self):
        print("我会飞")


xiaotianquan = XiaoTianQuan()

xiaotianquan.fly()

# 会调用子类中重写的方法
xiaotianquan.bark()

xiaotianquan.drink()
