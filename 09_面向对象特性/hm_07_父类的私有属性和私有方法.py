class A:
    def __init__(self):
        self.num1 = 100
        self.__num2 = 200

    def __test(self):
        print("私有方法 %d %d" % (self.num1,self.__num2))


class B:
    def demo(self):
        # 不能访问
        # # 1.访问父类的私有属性
        # print(self.__num2)
        # # 2.调用父类的私有方法
        # self.__test
        pass



# 创建一个子类对象
b = B()
print(b)
# 在外界不能访问对象的私有属性和私有方法
# print(b.__num2)
# print(b.__test)
