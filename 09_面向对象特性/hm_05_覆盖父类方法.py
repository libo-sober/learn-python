class Animal:
    def eat(self):
        print("吃---")

    def drink(self):
        print("喝---")

    def run(self):
        print("跑---")

    def sleep(self):
        print("睡---")


class Dog(Animal):
    # def eat(self):
    #     print("吃")
    #
    # def drink(self):
    #     print("喝")
    #
    # def run(self):
    #     print("跑")
    #
    # def sleep(self):
    #     print("睡")

    def bark(self):
        print("汪汪叫")


class XiaoTianQuan(Dog):
    def bark(self):
        print("叫的跟神一样...")

    def fly(self):
        print("我会飞")


# 创建一个动物对象
# wangcai = Dog()
#
# wangcai.eat()
# wangcai.run()
# wangcai.drink()
# wangcai.sleep()
# wangcai.bark()

xiaotianquan = XiaoTianQuan()

xiaotianquan.fly()
# 会调用子类中重写的方法
xiaotianquan.bark()
xiaotianquan.drink()
