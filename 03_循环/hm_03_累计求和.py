"""
需求
计算0到100之间所有数字的累计求和结果
"""
i = 0

sum = 0

while i <= 100:
    sum += i

    i += 1

print("sum=%d" % sum)