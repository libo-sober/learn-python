"""
需求
计算0到100之间所有偶数的和
注意：
     0也是偶数
"""
i = 0

sum = 0

while i <= 100:
    if i % 2 == 0:
        sum += i

    i += 1

print("sum=%d" % sum)