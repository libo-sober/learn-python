import matplotlib.pyplot as plt
import numpy as np

# 1.绘制画布
plt.figure()
# 2.进行绘制
# （1）准备数据
# 准备x,y数据
# 默认不支持负号，想要使用负号，要设置RC参数
# 设置RC参数
plt.rcParams['font.sans-serif'] = 'SimHei'  # 设置RC参数字体，让其支持中文
x = np.arange(-2*np.pi, 2*np.pi, 0.1)
y1 = np.sin(x)
y2 = np.cos(x)
# （2）进行绘制
# 绘制两个图形，需要绘制两次
plt.plot(x, y1)
plt.plot(x, y2)
# （3）进行图形添加和修饰
# 增加标题   默认不支持中文
plt.title('x的sin图和cos图的关系')
# 增加图例
plt.legend(['y=sinx', 'y=cosx'])
# 增加x轴，y轴标签
plt.xlabel('x值')
plt.ylabel('y值',rotation=0)
# 3.保存图像
plt.savefig("三角函数.png")
# 4.图形展示
plt.show()