import numpy as np
import pandas as pd


N_STATES = 6
ACTIONS = ['left', 'right']
state = 2


def build_q_table(n_states, actions):
    table = pd.DataFrame(
        np.zeros((n_states, len(actions))),  # q_table initial values
        columns=actions,  # actions's name
    )
    # print(table)    # show table
    return table


q_table = build_q_table(N_STATES, ACTIONS)
state_actions = q_table.iloc(state)

max = q_table.idxmax()
print(q_table)
print()
print(state_actions)
print()
print(max)