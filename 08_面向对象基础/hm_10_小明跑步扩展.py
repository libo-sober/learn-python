"""
需求
1.小明和小美都爱跑步
2.小明体重75.0公斤
3.小美体重45.0公斤
4.每次跑步会减肥0.5公斤
5.每次吃东西会增加1公斤
"""
class Person:
    def __init__(self, name ,weight):
        # self.属性 = 形参
        self.name = name
        self.weight = weight

    def __str__(self):
        return "我的名字叫 %s 体重是 %.2f 公斤" % (self.name, self.weight)

    def run(self):
        print("%s 爱跑步，跑步锻炼身体" % self.name)
        self.weight -= 0.5

    def eat(self):
        print("%s 是吃货，吃完这顿再减肥" % self.name)
        self.weight += 1


xiaoming = Person("小明", 75.0)

xiaoming.run()
print(xiaoming.weight)
xiaoming.eat()
print(xiaoming.weight)

# 输出的是小明最初的状态
print(xiaoming)

# 小美爱跑步
xiaomei = Person("小美", 45.0)
xiaomei.run()
xiaomei.eat()

print(xiaomei)
print(xiaoming)
