"""
需求
1.房子（House）有户型、总面积和家具名称列表
  新房子没有任何家具
2.家具（HouseItem）有名字和占地面积，其中
  席梦思（bed）占地4pingmi
  衣柜（chest）占地2平米
  餐桌（table）占地1.5平米
3.将以上三件家具添加到房子中
4.打印房子时，要求输出：户型、总面积、剩余面积、家具名称列表
剩余面积
1.在创建房子对象时，定义一个剩余面积的属性，初始值和总面积相等
2.在调用add_item方法，向房间添加家具时让剩余面积 -= 家具面积
"""
# 被使用的类先开发
class HouseItem:
    def __init__(self, name, area):
        self.name = name
        self.area = area

    def __str__(self):
        return "[%s] 占地 %.2f" % (self.name, self.area)


# 1.创建家具
bed = HouseItem("席梦思", 4.0)
chest = HouseItem("衣柜", 2.0)
table = HouseItem("餐桌", 1.5)

print(bed)
print(chest)
print(table)
