class Cat:
    def __init__(self, new_name):
        print("这是一个初始化方法")

        # self.属性名 = 属性的初始值
        self.name = new_name

    def eat(self):
        # 哪一个对象调用的方法，self就是哪一个对象的引用
        print("%s 爱吃鱼" % self.name)


# 使用类名（）创建对象时会自动调用初始化方法__init__
tom = Cat("Tom")
print(tom.name)

lazzy_cat = Cat("大懒猫")
lazzy_cat.eat()
