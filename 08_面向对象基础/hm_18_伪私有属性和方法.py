class Women:
    def __init__(self, name):
        self.name = name
        self.__age = 18

    def __secret(self):
        # 在对象的方法内部是可以访问对象的私有属性的
        print("%s 的年龄是 %d" % (self.name, self.__age))


# 在名称前面加上 _类名 => _类名__名称
xiaofang = Women("小芳")
# 私有属性在外界不能够被直接访问
print(xiaofang._Women__age)
# 私有方法同样不允许在外界进行访问
xiaofang._Women__secret()