"""
需求
小猫爱吃鱼，小猫要喝水
分析
1.定义一个猫类
2.定义两个方法eat和drink
3.按照需求--不需要定义属性
"""


class Cat:
    def eat(self):
        # 哪一个对象调用的方法，self就是哪一个对象的引用
        print("%s 爱吃鱼" % self.name)
    def drink(self):
        print("%s 要喝水" % self.name)


# 创建猫对象
tom = Cat()

# 可以使用 .对象名 利用赋值语句就可以了
# 懒猫和懒猫2不会有此属性，若需要则在懒猫中添加
# 很简单，但是不推荐使用，因为没有把属性封装在类的内部
tom.name = "Tom"

tom.eat()
tom.drink()
print(tom.name)

print(tom)

# 再创建一个猫对象
# 不是同一只猫
lazzy_cat = Cat()

# 给懒猫增加名字属性
lazzy_cat.name = "大懒猫"

lazzy_cat.eat()
lazzy_cat.drink()
print(lazzy_cat.name)

print(lazzy_cat)

lazzy_cat2 = lazzy_cat

# 同一只猫
lazzy_cat2.eat()
lazzy_cat2.drink()
print(lazzy_cat2.name)

print(lazzy_cat2)
