"""
需求
小猫爱吃鱼，小猫要喝水
分析
1.定义一个猫类
2.定义两个方法eat和drink
3.按照需求--不需要定义属性
"""


class Cat:
    def eat(self):
        print("小猫爱吃鱼")
    def drink(self):
        print("小猫要喝水")


# 创建猫对象
tom = Cat()

tom.eat()
tom.drink()

print(tom)

# 再创建一个猫对象
# 不是同一只猫
lazzy_cat = Cat()

lazzy_cat.eat()
lazzy_cat.drink()

print(lazzy_cat)

lazzy_cat2 = lazzy_cat

# 同一只猫
lazzy_cat2.eat()
lazzy_cat2.drink()

print(lazzy_cat2)
