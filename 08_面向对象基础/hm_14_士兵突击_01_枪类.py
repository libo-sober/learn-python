"""
士兵突击
一个对象的属性可以是另外一个类创建的对象
需求
1.士兵许三多有一把AK47
2.士兵可以开火
3.枪能够发射子弹
4.枪装填子弹--增加子弹数量
"""
class Gun:
    def __init__(self, mode):
        # 1.枪的型号
        self.mode = mode
        # 2.子弹的数量
        self.bullet_count = 0

    def add_bullet(self, count):
        self.bullet_count += count

    def shoot(self):
        if self.bullet_count > 0:
            self.bullet_count -= 1
            print("[%s] 突突突... [%d]" % (self.mode, self.bullet_count))
        else:
            print("[%s] 没有子弹..." % self.mode)


# 创建枪对象
ak47 = Gun("AK47")
ak47.add_bullet(50)
ak47.shoot()