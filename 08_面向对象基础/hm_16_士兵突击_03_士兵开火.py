"""
士兵突击
一个对象的属性可以是另外一个类创建的对象
需求
1.士兵许三多有一把AK47
2.士兵可以开火
3.枪能够发射子弹
4.枪装填子弹--增加子弹数量
"""
class Gun:
    def __init__(self, mode):
        # 1.枪的型号
        self.mode = mode
        # 2.子弹的数量
        self.bullet_count = 0

    def add_bullet(self, count):
        self.bullet_count += count

    def shoot(self):
        if self.bullet_count > 0:
            self.bullet_count -= 3
            print("[%s] 突突突... [%d]" % (self.mode, self.bullet_count))
        else:
            print("[%s] 没有子弹..." % self.mode)


class Soldier:
    def __init__(self, name):
        # 1.姓名
        self.name = name
        # 2.枪--新兵没有枪
        self.gun = None

    def fire(self):
        # 1.判断士兵是否有枪
        if self.gun is None:
            print("[%s] 还没有枪..." % self.name)
            return
        # 2.高喊口号
        print("冲啊...[%s]" % self.name)
        # 3.让枪装填子弹
        self.gun.add_bullet(50)
        # 4.让枪发射子弹
        self.gun.shoot()
# 创建枪对象
ak47 = Gun("AK47")

# 创建许三多
xusanduo = Soldier("许三多")
xusanduo.gun = ak47
xusanduo.fire()
print(xusanduo.gun)