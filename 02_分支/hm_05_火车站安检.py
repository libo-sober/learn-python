"""
需求
1.定义布尔型变量has_ticket表示是否有车票
2.定义整型变量knife_length表示刀的长度，单位：厘米
3.首先检查是否有车票，如果有才允许进入安检
4.安检时需要检查刀的长度，判断是否超过20厘米
    如果超过20厘米，提示刀的长度，不允许上车
    如果不超过20厘米，安检通过
5.如果没有车票，不允许进门
"""
has_ticket = True

knife_length = int(input("刀的长度（厘米）："))

if has_ticket:
    if knife_length > 20:
        print("刀长%d厘米，不允许上车！" % knife_length)
    else:
        print("安检通过！")
else:
    print("没有车票，不允许进门！")
