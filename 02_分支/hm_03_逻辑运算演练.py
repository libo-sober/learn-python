"""
1.定义一个整数变量age，编写代码判断年龄是否正确
    要求人的年龄在0到120之间
2.定义两个整数变量python_score、c_score编写代码判断成绩
    要求只要有一门成绩大于60分就算合格、
3.定义一个布尔型变量is_employee，编写代码判断是否是本公司员工
    如果不是提示不允许入内
"""
age = int(input("输入你的年龄："))

if 0 <= age <= 120:
    print("年龄输入正确！")
else:
    print("年龄输入错误！")

python_score = float(input("请输入你的python成绩："))

c_score = float(input("请输入你的C语言成绩："))

if python_score > 60 or c_score > 60:
    print("合格！")
else:
    print("不合格！")

is_employee = True

if is_employee:
    print("你可以进入！")
else:
    print("非公务入！")

if not is_employee:
    print("非公务入！")