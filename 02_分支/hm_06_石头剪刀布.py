"""
需求
1.从控制台输入要出的拳----石头（1）/剪刀（2）/布（3）
2.电脑随机出拳----假设电脑只出拳头，完成整体代码功能
3.比较胜负
    石头胜剪刀
    剪刀胜布
    布胜石头
"""
# 导入随机工具包
import random

player = int(input("请输入您要出的拳 石头（1）/剪刀（2）/布（3）："))

computer = random.randint(1,3)

print("玩家选择的拳头是 %d - 电脑出的拳是 %d" % (player,computer))

if player == 1:
    if computer == 1:
        print("平手！")
    elif computer == 2:
        print("玩家胜！")
    else:
        print("电脑胜！")
elif player == 2:
    if computer == 1:
        print("电脑胜！")
    elif computer == 2:
        print("平手！")
    else:
        print("玩家胜！")
else:
    if computer == 1:
        print("玩家胜！")
    elif computer == 2:
        print("电脑胜！")
    else:
        print("平手！")