# 加载tkinter模块
import tkinter

# 创建一个主窗口
win = tkinter.Tk()
# 设置标题
win.title(string="古诗鉴赏")
# 在窗口内创建一个Label控件
b = tkinter.Label(win, text="花间一壶酒，独酌无相亲。举杯邀明月，对影成三人。")
# pack()方法设置窗口的位置、大小等选项
b.pack()
# 开始窗口的事件循环
win.mainloop()
