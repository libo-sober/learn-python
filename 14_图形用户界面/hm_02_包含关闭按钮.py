from tkinter import *

win = Tk()
win.title(string="古诗鉴赏")
Label(win, text="山气日夕佳，飞鸟相与还。此中有真意，欲辨已忘言。").pack()
# 创建一个button控件并调用pack()方法设置其位置在窗口的底端
# padx=20,button的大小，20个像素
Button(win, padx=20, text="关闭", command=win.quit).pack(side="bottom")
win.mainloop()
