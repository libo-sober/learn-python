"""
需求
定义变量保存小明的个人信息
姓名：小明
年龄：18
性别：男
身高：1.75米
体重：75.0公斤
"""
# 在python中，定义变量是不需要指定变量的类型
# 在运行的时候，python解释器会根据赋值语句等号右侧的数据
# 自动推导出变量中保存数据的变量的准确类型
name = "小明"

age = 18

gender = True  # 男

height = 1.75

weight = 75.0

print(name)