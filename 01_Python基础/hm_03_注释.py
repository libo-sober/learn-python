# 这是第一个注释
# 注释时在#号后加一个空格
print("你好，李波！")

# 格式规范
# 单行代码后面有两个空格
print("Hello world!")  # 输出Hello world！

"""
这是一个多行注释
在这里可以写很多注释
"""

print("Hello world!")

print("3 ** 3 = ")
print(3 ** 3)

# 变量的定义
# 变量名 = 值
count = 3 ** 3
print(count)
