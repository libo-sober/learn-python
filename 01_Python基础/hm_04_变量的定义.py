qq_number = "123456"

qq_password = "123"

print(qq_number)

print(qq_password)

# 不同变量之间的计算
i = 10
j = 10.5
b = True  # True : 1  False : 0

print(i + j)
print(j + b)

first_name = "三"
last_name = "张"
# 字符串用 + 拼接
print(first_name + last_name)
# 字符串变量 可以和 整数 使用 * 重复拼接相同的字符串
print((last_name + first_name) * 10)

# input()
qq_password = input("请输入qq密码:")

print(qq_password)
# 强制类型转换
qq_password = int(qq_password)

print(type(qq_password))