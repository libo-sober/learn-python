info_tuple = ("张三", 18, 1.75)

# 使用迭代遍历
for my_info in info_tuple:

    # 使用格式字符串拼接my_info这个变量不方便
    # 因为元组中通常保存的数据类型是不同的
    # 除非能够元组中的数据类型，否则不会有很多需求会要你遍历元组
    print(my_info)