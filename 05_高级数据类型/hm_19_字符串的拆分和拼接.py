# 假设：一下内容是从网络上抓取的
# 要求：
#  1.将字符串中的全部空白部分去掉
#  2.再使用 " " 作为分隔符，拼接为一个整齐的字符串
poem_str = "登鹳雀楼\t王之涣\t白日依山尽，\t\n黄河入海流。\t\t欲穷千里目，\n更上一层楼。"

print(poem_str)

# 1.拆分字符串
# split方法返回一个列标
poem_list = poem_str.split()
print(poem_list)

# 2.合并字符串
# string.join(seq)以string作为分隔符，将seq中所有的元素（的字符串表示）合并为一个字符串
# 返回一个字符串
result = " ".join(poem_list)
print(result)