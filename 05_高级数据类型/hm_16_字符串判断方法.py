# 1.判断空白字符
space_str = "     \t\r\n"

print(space_str.isspace())

# 2.判断字符串中是否只包含数字
# 1>都不能判断小数
# num_str = "1.1"
# 2>unicode 字符串
# num_str = "\u00B2"
# 3>中文数字
num_str = "一千零一"

print(num_str)
print(num_str.isdecimal())  # 是否是小数

print(num_str.isdigit())  # 是否是数字

print(num_str.isnumeric())  # 是否是数字
