hello_str = "hello hello"

# 1.统计字符串的长度
print(len(hello_str))

# 2.统计某一个小（子）字符串出现的次数
print(hello_str.count("hello"))

# 3.某一个子字符串出现的位置
print(hello_str.index("ll"))
# 记录的是子字符串第一次出现的位置
# ValueError: substring not found,子字符串不存在
# print(hello_str.index("abc"))
