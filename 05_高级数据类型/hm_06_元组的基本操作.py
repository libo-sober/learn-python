info_tuple = ("张三", 18, 1.75, 18)

# 1.取值和取索引
print(info_tuple[0])
# 已经知道数据的内容，想知道该数据在元组中得到索引
print(info_tuple.index(18))
# 2.统计技术
print(info_tuple.count(18))
# 3.统计元组中元素的个数
print(len(info_tuple))