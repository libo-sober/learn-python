for num in [1, 2, 3, 4]:

    print(num)
    #
    # if num == 2:
    #
    #     break
else:
    # else中代码一定要在for完全遍历之后才会执行
    # 如果循环体内使用break退出循环，那么else中代码就不会被执行
    print("会执行吗？")

print("循环结束")