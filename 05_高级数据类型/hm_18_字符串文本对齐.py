# 假设：以下内容是从网络上抓取的
# 要求：顺序并且居中对齐输出以下内容
poem = ["\t\n登鹳雀楼",
        "王之涣",
        "白日依山尽，",
        "黄河入海流。\t\n",
        "欲穷千里目，",
        "更上一层楼。"]

for poem_str in poem:

    # 先使用strip方法去除空白字符串
    
    print(poem_str.strip().center(10, "　"))

for poem_str in poem:
    print(poem_str.strip().ljust(10, " "))
    # print(poem_str.ljust(10, "　"))

for poem_str in poem:
    print(poem_str.strip().rjust(10, " "))
    # print(poem_str.rjust(10, "　"))
