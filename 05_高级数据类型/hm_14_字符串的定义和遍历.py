str1 = "hello python"
# or str1 = 'hello python'

# 引号中有引号则用单引号定义字符串
str2 = '我的外号是"大西瓜"'

print(str1)

print(str2)

print(str1[6])

# 遍历
for c in str2:
    print(c)
