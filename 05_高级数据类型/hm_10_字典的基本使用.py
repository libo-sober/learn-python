xiaoming_dict = {"name": "小明"}

# 1.取值
# 不是下标，而是键的名称，如果不存在会报错
print(xiaoming_dict["name"])

# 2.增加/修改
# 如果key不存在，增加
xiaoming_dict["age"] = 18
# 如果key存在，修改
xiaoming_dict["name"] = "小小明"

# 3.删除
xiaoming_dict.pop("name")
# KeyError: 'name123'
# xiaoming_dict.pop("name123")

print(xiaoming_dict)
