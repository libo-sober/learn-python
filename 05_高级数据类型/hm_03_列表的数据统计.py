name_list = ["张三", "李四", "王五", "王小二", "张三"]

# len() 统计列表中的元素个数
print("列表中包含%d个元素" % len(name_list))

# count()
print("张三出现了%d次" % name_list.count("张三"))

# 从列表中删除数据,删除列表中第一个出现的数据，如果不存在，报错
name_list.remove("张三")

print(name_list)