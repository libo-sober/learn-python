name_list = ["张三", "李四", "王五"]

# 1.取值和取索引
print(name_list[0])
print(name_list.index("王五"))
# 2.修改
name_list[1] = "lisi"
# 3.增加
name_list.append("李波")

name_list.insert(2,"小妹妹")

temp_list = ["孙悟空", "猪八戒", "沙师弟"]

# extend 方法可以把其他列表中的完整内容追加到当前列表的末尾
name_list.extend(temp_list)
# 4.删除
name_list.remove("lisi")
# pop方法默认可以把列表中最后一个元素删除
name_list.pop()
# pop(2) 删除索引为2的元素
name_list.pop(2)
# clear 清空列表
# name_list.clear()

print(name_list)