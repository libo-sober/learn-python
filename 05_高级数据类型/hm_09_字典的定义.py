# 字典是一个无序的数据集合，使用print输出字典时
# 通常输出的顺序和定义的顺序是不一致的

xiaoming = {"name": "小明",
            "age": 18,
            "gender": "男",
            "height": 1.75,
            "weight": 75}

print(xiaoming)