students = [{"name": "阿土"},
            {"name": "小美"}
            ]

# 在学院列表中搜索指定的姓名
find_name = input("请输入要查找人的姓名：")

for stu_dict in students:

    print(stu_dict)

    if stu_dict["name"] == find_name:
        # 如果找到就应该退出循环，不用再进行遍历
        print("找到了 %s" % find_name)

        break

else:
    # 如果希望在搜索列表时，所有的字典检查后，都没有发现需要搜索的目标
    # 并且希望得到一个提示
    print("没有找到 %s" % find_name)

print("循环结束")