name_list = ["张三", "李四", "王五"]

# del关键字（delete）删除元素
del name_list[1]
# del关键字本质上是用来将一个从内存中删除
name = "小明"

del name
# NameError: name 'name' is not defined
# 注意： 如果用del关键字删除一个变量，在后续的开发代码中就不能使用了
# 在日常的开发中删除元素，建议用列表提供的删除方法
print(name)

print(name_list)
