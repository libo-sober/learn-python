"""
num_str = "0123456789"
需求：
1.截取从2到5位置的字符串
print(num_str[2:5])
2.截取从2到末尾的字符串
print(num_str[2:])
3.截取从开始到5位置的字符串
print(num_str[0:5])
print(num_str[:5])
4.截取完整的字符串
print(num_str)
print(num_str[:])
print(num_str[0:])
5.从开始位置每隔1个字符截取字符串
print(num_str[::2])
num_str[头：尾：步长]
6.从索引1开始每隔1个取1个
print(num_str[1::2])
7.从索引2到末尾-1的字符串
print(num_str[2:])
8.截取字符串末尾的两个字符
倒序索引
-1 -2 -3 ...
9.字符串的逆序（面试题）
倒序输出从-1开始，步长为-1
-1+（-1）=-2
-2+（-1）=-3
...
"""
num_str = "0123456789"

print("1")
print(num_str[2:5])

print("2")
print(num_str[2:])

print("3")
print(num_str[0:5])
print(num_str[:5])

print("4")
print(num_str[:])
print(num_str[0:])

print("5")
print(num_str[::2])

print("6")
print(num_str[1::2])

# 倒序索引
print("7")
print(num_str[-1])
print(num_str[2:-1])

print(num_str[-2:])

# 步长为-1，从指定开始的位置向左以步长1切
print("8")
print(num_str[-1::-1])